﻿using System;
using System.Net;
using System.IO;

using Newtonsoft.Json.Linq;
namespace JuegoConsola
{
    public class Clima
    {
        public static string devolverClima()
        {
            string url = "https://query.yahooapis.com/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22buenos%20aires%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
           
                    string dato = "";
                    bool err = false;
                    StreamReader sr = null;
                    JObject data = null;
                    try
                    {
                        WebRequest req = WebRequest.Create(url);

                        WebResponse respuesta = req.GetResponse();

                        Stream stream = respuesta.GetResponseStream();

                        sr = new StreamReader(stream);
                    }
                    catch (Exception e)
                    {
                        err = true;
                    }

                    if (!err)
                    {
                        data = JObject.Parse(sr.ReadToEnd());



                        
                           
                            try
                            {
                                dato = (string)data["query"]["results"]["channel"]["item"]["condition"]["text"];
                            }
                            catch (Exception e)
                            {
                                dato = null;
                            }


                        

                    }

            string result = "";

                    if (err)
                    {
                result = "error";
                    }
                    else
                    {
                        if (dato != null)
                        {

                    result = dato;
                        }
                        else
                        {

                    result = "error";
                        }
                    }

            return result;

            
        }

        public static void CambiarColor(string clima)
        {
            switch (clima)
            {
                
                case "error":
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
                case "Cloudy":
                    Console.BackgroundColor = ConsoleColor.Magenta;
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
                case "Sunny":
                    Console.BackgroundColor = ConsoleColor.Yellow;
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
                case "Rain":
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
                default:
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;



            }

        }
               
           

       

        }
    }

