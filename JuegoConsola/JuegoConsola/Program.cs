﻿using System;
using System.IO;



namespace JuegoConsola
{
    class Program
    {
        static void Main(string[] args)
        {
            bool primerloop = true;
            while (primerloop)
            {
                Console.Clear();
                bool continuarLoop = true;
                Console.CursorVisible = false;

                Lives.Reset();



                Clima.CambiarColor(Clima.devolverClima());



                FileStream fs;
                if (!File.Exists("gameData.bla"))
                {
                    fs = File.Create("gameData.bla");

                    BinaryWriter bw = new BinaryWriter(fs);

                    Console.WriteLine("INGRESAR MENSAJE DE BIENVENIDA");

                    string text = Console.ReadLine();
                    bw.Write(text);

                    bw.Write("Bienvenido. Presione 1 para jugar de a 1 jugador o 2 para jugar de a 2 jugadores.");

                    bw.Close();

                    fs.Close();

                }
                Console.Clear();
                fs = File.OpenRead("gameData.bla");

                BinaryReader br = new BinaryReader(fs);
                string texto = br.ReadString();
                Console.WriteLine(texto);
                texto = br.ReadString();
                Console.WriteLine(texto);

                br.Close();

                fs.Close();

                ConsoleKeyInfo key;
                do
                {
                    key = Console.ReadKey();
                } while (key.Key != ConsoleKey.NumPad1 && key.Key != ConsoleKey.NumPad2);

                Jugador[] jugadores;
                if (key.Key == ConsoleKey.NumPad1)
                {
                    jugadores = new Jugador[1];
                    jugadores[0] = new Jugador(0, 0, 1);
                }
                else
                {
                    jugadores = new Jugador[2];
                    jugadores[0] = new Jugador(0, 0, 1);
                    jugadores[1] = new Jugador(5, 5, 2);
                }
                int cantMinas = 20;
                Mina[] minas = new Mina[cantMinas];
                Random rand = new Random();
                for (int i = 0; i < cantMinas; i++)
                {
                    Mina m = new Mina();
                    int x = rand.Next(3, 75);
                    int y = rand.Next(2, 20);
                    m.setLocation(x, y);
                    minas[i] = m;
                }

                int cantEnemigos = 8;
                Enemigo[] enemigos = new Enemigo[cantEnemigos];

                for (int i = 0; i < cantEnemigos; i++)
                {
                    int x = rand.Next(3, 75);
                    int y = rand.Next(2, 20);
                    Enemigo e = new Enemigo(x, y);
                    enemigos[i] = e;
                }
                Lives lives = new Lives();
                Console.Clear();

                //Los //* indican lineas de codigo que te agrego
               //*

                while (continuarLoop)
                {
                    Console.Clear();//*
                    lives.Draw(Lives.lives);

                    foreach (Mina m in minas)
                    {
                        m.LoopMina();
                    }
                    foreach (Enemigo e in enemigos)
                    {
                        e.LoopEnemigo();
                    }
                    foreach (Jugador j in jugadores)
                    {
                        bool semovio = j.LoopJugador();
                        if (semovio)
                        {
                            j.chequearMuerte(minas, enemigos);
                        }
                    }




                    if (Lives.lives == 0) continuarLoop = false;

                    System.Threading.Thread.Sleep(100);//*

                }//FIN LOOP

                Console.Clear();
                Console.WriteLine("GAME OVER");
                Console.WriteLine("TOQUE ENTER PARA SEGUIR JUGANDO");
                Console.WriteLine("TOQUE ESCAPE PARA SALIR DEL JUEGO");
                ConsoleKeyInfo tecla;
                do
                {
                    tecla = Console.ReadKey();
                } while (tecla.Key != ConsoleKey.Enter && tecla.Key != ConsoleKey.Escape);
               
                if(tecla.Key == ConsoleKey.Escape) { primerloop = false; }
            }
        }
    }
}
