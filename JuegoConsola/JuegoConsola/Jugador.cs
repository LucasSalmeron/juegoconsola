﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuegoConsola
{
    class Jugador
    {
        ConsoleKeyInfo teclaUsada;
        int locationX = 0;
        int locationY = 0;
        int exlocationX;
        int exlocationY;

        ConsoleKey izq = ConsoleKey.E;
        ConsoleKey der = ConsoleKey.E;
        ConsoleKey arr = ConsoleKey.E;
        ConsoleKey aba = ConsoleKey.E;

        
        
        public Jugador(int x, int y, int id)
        {
            locationX = x;
            locationY = y;
            exlocationX = locationX + 1;
            exlocationY = locationY + 1;

            switch (id)
            {
                case 1:
                    izq = ConsoleKey.LeftArrow;
                    der = ConsoleKey.RightArrow;
                    arr = ConsoleKey.UpArrow;
                    aba = ConsoleKey.DownArrow;
                    break;

                case 2:
                    izq = ConsoleKey.A;
                    der = ConsoleKey.D;
                    arr = ConsoleKey.W;
                    aba = ConsoleKey.S;
                    break;

            }

        }


        public bool LoopJugador()
        {
            bool semovio = false;
            if (Console.KeyAvailable)
            {
                // We have input, process accordingly
                teclaUsada = Console.ReadKey(true);
                exlocationX = locationX;
                exlocationY = locationY;

                if(teclaUsada.Key == izq)
                {

                    if (locationX > 0)
                    {
                        // Move ourself left
                        locationX = locationX - 1;
                        semovio = true;
                    }
                }

                if(teclaUsada.Key == der)
                {
                    if (locationX < 78)
                    {
                        // Read the System Caret section for
                        // more information on why you should 
                        // use 78 instead of the 79 here.
                        locationX = locationX + 1;
                        semovio = true;
                    }

                }

                if(teclaUsada.Key == aba)
                {
                    if (locationY < 24)
                    {
                        // Move ourself down
                        locationY = locationY + 1;
                        semovio = true;
                    }
                }

                if(teclaUsada.Key == arr)
                {
                    if (locationY > 0)
                    {
                        // Move ourself up
                        locationY = locationY - 1;
                        semovio = true;
                    }

                }

                //te conviene hacer una clase bala y crearla aca.
                // dispara para el mismo lado siempre
                if (teclaUsada.Key == ConsoleKey.Spacebar)
                {
                    for (int i = GetLocationX(); i < Console.WindowWidth-1; i++)
                    {
                        Console.SetCursorPosition(i, GetLocationY());
                        Console.Write("*");
                    }
                }


            }

            Console.SetCursorPosition(exlocationX, exlocationY);
            Console.Write(" ");
            // Draw the player
            Console.SetCursorPosition(locationX, locationY);
            Console.Write("@");


            return semovio;


        }

        public void chequearMuerte(Mina[] minas, Enemigo[] enemigos)
        {
            foreach (Mina m in minas)
            {
                if (this.GetLocationX() == m.getX() && this.GetLocationY() == m.getY())
                {
                    Lives.lives--;
                    this.SetLocationX(0);
                    this.SetLocationY(0);
                }
            }
            foreach(Enemigo e in enemigos)
            {
                if (this.GetLocationX() == e.getX() && this.GetLocationY() == e.getY())
                {
                    Lives.lives--;
                    this.SetLocationX(0);
                    this.SetLocationY(0);
                }
            }
        }

        //te agrego los getters
        public int GetLocationX()
        {
            return locationX;
        }

        public int GetLocationY()
        {
            return locationY;
        }
        public void SetLocationX(int x)
        {
            locationX = x;
        }

        public void SetLocationY(int y)
        {
            locationY = y;
        }
    }
}
